package com.example.squizit.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.squizit.MyApplication
import com.example.squizit.data.Repository
import com.example.squizit.data.models.Category
import com.example.squizit.data.models.response.CategoryResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CategoryViewModel:ViewModel() {

    @Inject
    lateinit var repository: Repository

    init {
        MyApplication.instance.appComponent.inject(this)
    }

    private val disposable = CompositeDisposable()

    val categories = MutableLiveData<List<Category>>()
    val categoriesErrorMsg = MutableLiveData<String>()
    val loading = MutableLiveData<Boolean>()

    fun loadCategories(){
        fetchCategories()
    }

    private fun fetchCategories(){
        loading.value = true
        disposable.add(
                repository.getCategories()
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object: DisposableSingleObserver<CategoryResponse>(){
                            override fun onSuccess(value: CategoryResponse) {
                                categories.value = value.categories
                                categoriesErrorMsg.value = ""
                                loading.value = false
                            }

                            override fun onError(e: Throwable) {
                                categoriesErrorMsg.value = e.message
                                loading.value = false
                            }
                        })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}