package com.example.squizit.viewModel

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.squizit.MyApplication
import com.example.squizit.data.Repository
import com.example.squizit.data.models.Question
import com.example.squizit.data.models.response.QuestionsResponse
import com.example.squizit.utils.Extras
import com.example.squizit.utils.Prefs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class QuestionViewModel : ViewModel() {

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var prefs: Prefs

    init {
        MyApplication.instance.appComponent.inject(this)
    }

    private val disposable = CompositeDisposable()

    var questions = MutableLiveData<List<Question>>()
    var questionsErrorMsg = MutableLiveData<String>()
    var loading = MutableLiveData<Boolean>()

    fun loadQuestions(id: String) {
        fetchQuestions(id)
    }

    @SuppressLint("DefaultLocale")
    private fun fetchQuestions(categoryId: String) {
        loading.value = true
        disposable.add(
                repository.getQuestions(categoryId, getParameterLevel(prefs.getLevel(Extras.EXTRAS_LEVEL)!!), getParameterType(prefs.getType(Extras.EXTRAS_TYPE)!!))
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableSingleObserver<QuestionsResponse>() {
                            override fun onSuccess(t: QuestionsResponse) {
                                questions.value = t.questions
                                loading.value = false
                            }

                            override fun onError(e: Throwable) {
                                questionsErrorMsg.value = e.message
                                loading.value = false
                            }
                        }))
    }

    @SuppressLint("DefaultLocale")
    private fun getParameterLevel(level: String) = level.toLowerCase()

    private fun getParameterType(type: String): String {
        return if (type == "Multiple Choice") {
            "multiple"
        } else {
            "boolean"
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}