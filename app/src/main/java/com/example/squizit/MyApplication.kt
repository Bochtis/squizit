package com.example.squizit

import android.app.Application
import com.example.squizit.di.AppModule
import com.example.squizit.di.DaggerAppComponent
import com.example.squizit.di.AppComponent

class MyApplication : Application() {

    lateinit var appComponent: AppComponent

    companion object {
        lateinit var instance: MyApplication
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        setupAppComponent()
    }

    private fun setupAppComponent() {
        appComponent = DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()
        appComponent.inject(this)
    }
}
