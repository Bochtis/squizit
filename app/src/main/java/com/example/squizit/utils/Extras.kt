package com.example.squizit.utils

class Extras {

    companion object{
        const val EXTRAS_LEVEL = "level"
        const val EXTRAS_TYPE = "type"
    }

}