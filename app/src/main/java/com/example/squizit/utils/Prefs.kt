package com.example.squizit.utils

import android.content.Context
import android.content.SharedPreferences

class Prefs(private val context: Context) {
    private val PREFS_NAME = "kotlincodes"
    private val sharedPreferences: SharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    fun setLevel(KEY_NAME: String, value: String?) {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString(KEY_NAME, value)
        editor.apply()
    }

    fun getLevel(KEY_NAME: String): String? {
        return sharedPreferences.getString(KEY_NAME, "easy")
    }

    fun setType(KEY_NAME: String, value: String?) {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString(KEY_NAME, value)
        editor.apply()
    }

    fun getType(KEY_NAME: String): String? {
        return sharedPreferences.getString(KEY_NAME, "multiple")
    }
}