package com.example.squizit.view.question

import android.animation.Animator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.example.squizit.R
import com.example.squizit.data.models.Answer
import com.example.squizit.data.models.Question
import com.example.squizit.utils.Extras
import com.example.squizit.utils.Prefs
import com.example.squizit.viewModel.QuestionViewModel
import kotlinx.android.synthetic.main.fragment_question.*

class QuestionFragment : Fragment() {

    private lateinit var args: QuestionFragmentArgs
    private lateinit var viewModel: QuestionViewModel
    private lateinit var prefs: Prefs
    private var questionIndex = -1
    private lateinit var questionsList: List<Question>
    private var answersList = mutableListOf<String>()
    private var answerItemList = mutableListOf<Answer>()
    private var correctAnswers = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_question, container, false)

        prefs = Prefs(context!!)
        args = QuestionFragmentArgs.fromBundle(arguments!!)
        viewModel = ViewModelProviders.of(this).get(QuestionViewModel::class.java)
        viewModel.loadQuestions(args.categoryId.toString())

        (activity as AppCompatActivity).supportActionBar?.title = args.categoryName

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        question_number_txt_view.text = "(1/5)"

        true_btn.setOnClickListener {
            it.background = ResourcesCompat.getDrawable(resources, R.drawable.custom_btn, null)
            true_btn_txt_view.setTextColor(ResourcesCompat.getColor(resources, R.color.white, null))
            false_btn.background = ResourcesCompat.getDrawable(resources, R.drawable.custom_white_btn, null)
            false_btn_txt_view.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
            enableNextBtn()
            checkAnswer("True")
        }

        false_btn.setOnClickListener {
            it.background = ResourcesCompat.getDrawable(resources, R.drawable.custom_btn, null)
            false_btn_txt_view.setTextColor(ResourcesCompat.getColor(resources, R.color.white, null))
            true_btn.background = ResourcesCompat.getDrawable(resources, R.drawable.custom_white_btn, null)
            true_btn_txt_view.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
            enableNextBtn()
            checkAnswer("False")
        }

        next_txt_view.setOnClickListener {
            loadNextQuestion()
        }

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.questions.observe(this, Observer { questions ->
            questions?.let {
                if (it.isNotEmpty()) {
                    questionsList = it
                    setUpQuestions()
                    configureAnswerView()
                    disableNextBtn()
                    container_constraint_layout.visibility = View.VISIBLE
                    slideInAnimation()
                } else {
                    empty_msg_txt_view.visibility = View.VISIBLE
                }
            }
        })

        viewModel.questionsErrorMsg.observe(this, Observer { questions ->
            questions?.let {
                if (it.isNotEmpty()) {
                    error_msg_txt_view.text = it
                    error_msg_txt_view.visibility = View.VISIBLE
                }
            }
        })

        viewModel.loading.observe(this, Observer { loading ->
            loading?.let {
                if (it) {
                    progress_rel.visibility = View.VISIBLE
                } else {
                    progress_rel.visibility = View.GONE
                }
            }
        })
    }

    private fun setUpQuestions() {
        questionIndex = 0
        question_txt_view.text = HtmlCompat.fromHtml(questionsList[questionIndex].question, HtmlCompat.FROM_HTML_MODE_LEGACY)
        card_view.visibility = View.VISIBLE
        next_txt_view.visibility = View.VISIBLE
    }

    private fun configureAnswerView() {
        if (isMultipleChoice()) {
            multiple_choice_recycler.visibility = View.VISIBLE
            true_btn.visibility = View.GONE
            false_btn.visibility = View.GONE

            loadAnswers()
            setUpList()
        } else {
            multiple_choice_recycler.visibility = View.GONE
            true_btn.visibility = View.VISIBLE
            false_btn.visibility = View.VISIBLE
        }
    }

    private fun loadAnswers() {
        val question = questionsList[questionIndex]
        answersList.clear()
        answersList.add(0, question.answer)
        for (i in 0 until question.incorrect_answers.size) {
            answersList.add(i + 1, question.incorrect_answers[i])
        }
        // shuffle answers
        answersList.shuffle()

        answerItemList.clear()
        for (i in 0 until answersList.size) {
            answerItemList.add(i, Answer(false, answersList[i]))
        }
    }

    private fun setUpList() {

        val answersAdapter = QuestionRecyclerAdapter(answerItemList) { position ->
            checkAnswer(answersList[position])
            enableNextBtn()
        }
        val layoutMng = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        multiple_choice_recycler.apply {
            adapter = answersAdapter
            layoutManager = layoutMng
            isNestedScrollingEnabled = false
        }
    }

    private fun loadNextQuestion() {
        if (questionIndex < 4) {
            questionIndex++
            question_txt_view.text = HtmlCompat.fromHtml(questionsList[questionIndex].question, HtmlCompat.FROM_HTML_MODE_LEGACY)
            disableNextBtn()
            clearBtns()

            (activity as AppCompatActivity).supportActionBar?.title = args.categoryName
            question_number_txt_view.text = String.format("(%s/5)", questionIndex + 1)

            if (isMultipleChoice()) {
                slideOutAnimation()
            }
        } else {
            val bundle = Bundle()
            bundle.putInt("score", correctAnswers)
            Navigation.findNavController(view!!).navigate(R.id.action_questionFragment_to_resultsFragment, bundle)
        }
    }

    private fun loadNextAnswers() {
        loadAnswers()
        (multiple_choice_recycler.adapter as QuestionRecyclerAdapter).updateAnswers(answerItemList)
    }

    private fun enableNextBtn() {
        next_txt_view.setTextColor(ContextCompat.getColor(context!!, R.color.colorAccent))
        next_txt_view.isClickable = true
        next_txt_view.isFocusable = true
    }

    private fun disableNextBtn() {
        next_txt_view.setTextColor(ContextCompat.getColor(context!!, R.color.transparentColorAccent))
        next_txt_view.isClickable = false
        next_txt_view.isFocusable = false
    }

    private fun checkAnswer(userAnswer: String) {
        val answer = questionsList[questionIndex].answer
        if (answer == userAnswer) correctAnswers++
    }

    private fun clearBtns() {
        true_btn.background = ResourcesCompat.getDrawable(resources, R.drawable.custom_white_btn, null)
        true_btn_txt_view.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        false_btn.background = ResourcesCompat.getDrawable(resources, R.drawable.custom_white_btn, null)
        false_btn_txt_view.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
    }

    private fun isMultipleChoice() = prefs.getType(Extras.EXTRAS_TYPE) == resources.getStringArray(R.array.types)[0]

    private fun slideInAnimation() {
        YoYo.with(Techniques.SlideInLeft)
                .duration(800)
                .pivot(YoYo.CENTER_PIVOT, YoYo.CENTER_PIVOT)
                .interpolate(AccelerateDecelerateInterpolator())
                .playOn(multiple_choice_recycler)
    }

    private fun slideOutAnimation() {
        YoYo.with(Techniques.SlideOutRight)
                .duration(800)
                .pivot(YoYo.CENTER_PIVOT, YoYo.CENTER_PIVOT)
                .interpolate(AccelerateDecelerateInterpolator())
                .withListener(object : Animator.AnimatorListener {
                    override fun onAnimationCancel(animation: Animator?) {
                    }

                    override fun onAnimationStart(animation: Animator?) {
                    }

                    override fun onAnimationRepeat(animation: Animator?) {
                    }

                    override fun onAnimationEnd(animation: Animator?) {
                        loadNextAnswers()
                        slideInAnimation()
                    }
                })
                .playOn(multiple_choice_recycler)
    }

}