package com.example.squizit.view.splash

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast

import com.example.squizit.R
import com.example.squizit.view.home.MainActivity

import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        sign_in_btn.setOnClickListener(this)
        sign_up_btn.setOnClickListener(this)
        guest_btn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            sign_in_btn -> Toast.makeText(this, "Signing in...", Toast.LENGTH_SHORT).show()
            sign_up_btn -> Toast.makeText(this, "Sign up...", Toast.LENGTH_SHORT).show()
            guest_btn -> MainActivity.show(this)
        }
    }
}
