package com.example.squizit.view.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.squizit.R
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment(), View.OnClickListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        one_player_btn.setOnClickListener(this)
        two_players_btn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            one_player_btn -> Navigation.findNavController(v!!).navigate(R.id.action_homeFragment_to_categoriesFragment)
            two_players_btn -> Navigation.findNavController(v!!).navigate(R.id.action_homeFragment_to_categoriesFragment)
        }
    }
}
