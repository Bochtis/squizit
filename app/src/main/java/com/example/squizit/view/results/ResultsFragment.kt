package com.example.squizit.view.results

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.squizit.R
import kotlinx.android.synthetic.main.fragment_results.*

class ResultsFragment : Fragment() {

    private lateinit var args: ResultsFragmentArgs
    private var score = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_results, container, false)

        args = ResultsFragmentArgs.fromBundle(arguments!!)
        score = args.score

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        when (score) {
            1 -> title_txt_view.text = "Are you kidding me mate?"
            2 -> title_txt_view.text = "Keep trying buddy!"
            3 -> title_txt_view.text = "Well done!!"
            4 -> title_txt_view.text = "Great job!!!"
            5 -> title_txt_view.text = "Congratulations, perfect score!!!!"
        }

        results_txt_view.text = String.format("Your score is $score/5")

        play_again_txt_view.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_resultsFragment_to_categoriesFragment)
        }
    }

}