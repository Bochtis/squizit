package com.example.squizit.view.question

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.squizit.R
import com.example.squizit.data.models.Answer
import kotlinx.android.synthetic.main.category_item.view.*

class QuestionRecyclerAdapter(var answers: MutableList<Answer>, val itemClick: (Int) -> Unit) : RecyclerView.Adapter<QuestionRecyclerAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder = MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.question_item, parent, false)
    )

    override fun getItemCount(): Int = answers.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(answers[position])
    }

    fun updateAnswers(newAnswers: MutableList<Answer>) {
        answers = newAnswers
        notifyDataSetChanged()
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(answer: Answer) {
            itemView.title.text = answer.title
            itemView.setOnClickListener {
                itemClick(layoutPosition)
                answer.isSelected = true
                answers.forEach {
                    if (it != answer && it.isSelected) {
                        it.isSelected = false
                    }
                }
                notifyDataSetChanged()
            }

            if (answer.isSelected) {
                itemView.title.background = ResourcesCompat.getDrawable(itemView.resources, R.drawable.custom_btn, null)
                itemView.title.setTextColor(ContextCompat.getColor(itemView.context, R.color.generic_txt))
            } else {
                itemView.title.background = ResourcesCompat.getDrawable(itemView.resources, R.drawable.custom_white_btn, null)
                itemView.title.setTextColor(ContextCompat.getColor(itemView.context, R.color.colorPrimary))
            }
        }
    }
}