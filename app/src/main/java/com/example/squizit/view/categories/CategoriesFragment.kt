package com.example.squizit.view.categories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.squizit.R
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.squizit.utils.GridSpacingItemDecoration
import com.example.squizit.viewModel.CategoryViewModel
import kotlinx.android.synthetic.main.fragment_categories.*

class CategoriesFragment : Fragment() {

    lateinit var viewModel: CategoryViewModel
    private val categoriesAdapter = CategoryRecyclerAdapter(arrayListOf())

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_categories, container, false)

        viewModel = ViewModelProviders.of(this).get(CategoryViewModel::class.java)
        viewModel.loadCategories()

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpRecycler()

        observeViewModel()
    }

    private fun setUpRecycler() {
        categories_recycler.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            isNestedScrollingEnabled = false
            adapter = categoriesAdapter
        }
    }

    private fun observeViewModel() {
        viewModel.categories.observe(this, Observer { categories ->
            categories?.let {
                if (it.isNotEmpty()) {
                    categories_recycler.visibility = View.VISIBLE
                    categoriesAdapter.updateCategories(it)
                }
            }
        })

        viewModel.categoriesErrorMsg.observe(this, Observer { error ->
            error?.let {
                if (it.isNotEmpty()) {
                    error_msg_txt_view.text = it
                    error_msg_txt_view.visibility = View.VISIBLE
                }
            }
        })

        viewModel.loading.observe(this, Observer { loading ->
            loading?.let {
                if (it) {
                    progress_rel.visibility = View.VISIBLE
                } else {
                    progress_rel.visibility = View.GONE
                }
            }
        })
    }
}
