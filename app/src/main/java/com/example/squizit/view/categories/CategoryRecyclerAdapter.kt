package com.example.squizit.view.categories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.squizit.R
import com.example.squizit.data.models.Category
import kotlinx.android.synthetic.main.category_item.view.*

class CategoryRecyclerAdapter(var categories: ArrayList<Category>) : RecyclerView.Adapter<CategoryRecyclerAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder = MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.category_item, parent, false)
    )

    override fun getItemCount(): Int = categories.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(categories[position])
    }

    fun updateCategories(newCategories: List<Category>){
        categories.clear()
        categories.addAll(newCategories)
        notifyDataSetChanged()
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(category: Category) {
            itemView.title.text = category.name
            itemView.setOnClickListener {
                val bundle = Bundle()
                bundle.putInt("categoryId", category.id)
                bundle.putString("categoryName", category.name)
                Navigation.findNavController(it).navigate(R.id.action_categoriesFragment_to_questionFragment, bundle)
            }
        }

    }
}