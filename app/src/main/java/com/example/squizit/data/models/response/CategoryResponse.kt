package com.example.squizit.data.models.response

import com.example.squizit.data.models.Category
import com.squareup.moshi.Json

class CategoryResponse {
    @Json(name = "trivia_categories")
    var categories: List<Category>? = null
}
