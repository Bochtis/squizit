package com.example.squizit.data.rest;

public class Urls {

    public static final String API_BASE_URL = "https://opentdb.com/";

    static final String CATEGORIES = "api_category.php";
    static final String QUESTIONS = "api.php?amount=5";
}
