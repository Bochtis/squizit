package com.example.squizit.data.models

data class Answer(
        var isSelected: Boolean,
        val title: String
)
