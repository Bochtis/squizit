package com.example.squizit.data

import com.example.squizit.MyApplication
import com.example.squizit.data.models.response.CategoryResponse
import com.example.squizit.data.models.response.QuestionsResponse
import com.example.squizit.data.rest.ApiCallInterface
import io.reactivex.Single
import javax.inject.Inject

class Repository {

    @Inject
    lateinit var apiCallInterface: ApiCallInterface

    init {
        MyApplication.instance.appComponent.inject(this)
    }

    fun getCategories(): Single<CategoryResponse> = apiCallInterface.getCategories()

    fun getQuestions(categoryId: String, difficulty: String, type: String): Single<QuestionsResponse> = apiCallInterface.getQuestions(categoryId, difficulty, type)

}
