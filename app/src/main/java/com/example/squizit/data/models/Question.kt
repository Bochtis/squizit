package com.example.squizit.data.models

import com.squareup.moshi.Json

class Question(
        @Json(name = "question")
        var question: String,
        @Json(name = "correct_answer")
        var answer: String,
        @Json(name = "incorrect_answers")
        var incorrect_answers: List<String>
)