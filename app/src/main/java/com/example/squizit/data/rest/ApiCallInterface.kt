package com.example.squizit.data.rest

import com.example.squizit.data.models.response.CategoryResponse
import com.example.squizit.data.models.response.QuestionsResponse

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiCallInterface {

    @GET(Urls.CATEGORIES)
    fun getCategories(): Single<CategoryResponse>

    @GET(Urls.QUESTIONS)
    fun getQuestions(@Query("category") category: String, @Query("difficulty") difficulty: String, @Query("type") type: String): Single<QuestionsResponse>
}
