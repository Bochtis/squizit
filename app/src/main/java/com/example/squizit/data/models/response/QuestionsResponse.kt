package com.example.squizit.data.models.response

import com.example.squizit.data.models.Question
import com.squareup.moshi.Json

class QuestionsResponse(
    @Json(name = "results")
    var questions: List<Question>
)