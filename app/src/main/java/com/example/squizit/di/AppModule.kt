package com.example.squizit.di

import android.app.Application
import android.content.Context
import com.example.squizit.utils.Prefs

import javax.inject.Singleton

import dagger.Module
import dagger.Provides

@Module
class AppModule(val app: Application) {

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun providesPrefs(): Prefs = Prefs(app.baseContext)
}
