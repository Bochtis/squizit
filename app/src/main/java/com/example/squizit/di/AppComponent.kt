package com.example.squizit.di

import android.app.Application
import com.example.squizit.data.Repository
import com.example.squizit.viewModel.CategoryViewModel
import com.example.squizit.viewModel.QuestionViewModel

import javax.inject.Singleton

import dagger.Component

@Component(modules = [AppModule::class, UtilsModule::class])
@Singleton
interface AppComponent {
    fun inject(app: Application)
    fun inject(repository: Repository)
    fun inject(viewModel: CategoryViewModel)
    fun inject(viewModel: QuestionViewModel)
}
